----------------------------------------------------------------------------------------------------
-- rough.bs
----------------------------------------------------------------------------------------------------

-- parameter --
function setParam(v, ...)
	local t={...}
	return function()
		if type(v) == "table" then
			return unpack(v)
		else
			return v, unpack(t)
		end
	end
end

	pm={
		{"interval", 0, 100, 10},
		{"rnd_size", 0, 100, 0},
		{"rnd_pos", 0, 100, 0},
		{"size_p", 0, 100, 25},
		{"alpha_p", 0, 100, 0},
		{"dot_pmin", 0, 100, 0},
		{"dot_p", 0, 100, 0},
		{"dot_size", 0, 100, 0},
		{"dot_round", 0, 100, 0},
		{"dot_alpha", 0, 100, 0}
	}

	pm.ja={
		"間隔", "rnd_サイズ", "rnd_位置", "サイズ_p", "透明度_p",
		"点_p最小", "点_p", "点_サイズ", "点_角丸", "点_透明度"
	}

	for i in ipairs(pm) do

		local eval=function(s, ...)
			assert(loadstring(string.format(s, ...)))()
		end

		if bs_lang() == "ja" then
			if pm.ja then
				pm[i][1]=pm.ja[i]
			end
		end

		eval('param%d=setParam(pm[%d])', i, i)
		eval('pm[%d].v=bs_param%d()', i, i)

	end

	default_size=setParam(20, 0)

	bs_setmode(1)

	firstDraw=true
	lastX, lastY=0, 0

	lastRndAngle=0
	lastPoint=0

-- sub --
function drawPolygon(x, y, w, hFlat, angle, point, retio, line, r, g, b, a)

	local bezAngle=math.rad(360/point)

	if point == 3 then
		retio=(1-bezAngle/math.rad(360))*(retio*0.96+1)-0.12^(1+retio)
	elseif point == 4 then
		retio=(1-bezAngle/math.rad(360))*(retio*0.6+1)-0.02
	elseif point == 5 then
		retio=(1-bezAngle/math.rad(360))*(retio*0.4+1)+0.02
	elseif point == 6 then
		retio=(1-bezAngle/math.rad(360))*(retio*0.2+1)+0.045
	end

	local x0, y0=math.cos(bezAngle), math.sin(bezAngle)

	bs_bezier_begin(x0*w/2, y0*w/2)

	for i=0, point-1 do

		local x1, y1=bs_rotate(math.cos(bezAngle*1.33)*retio*w/2, math.sin(bezAngle*1.33)*retio*w/2, bezAngle*i)
		local x2, y2=bs_rotate(math.cos(bezAngle*1.66)*retio*w/2, math.sin(bezAngle*1.66)*retio*w/2, bezAngle*i)
		local x3, y3=bs_rotate(math.cos(bezAngle*2)*w/2, math.sin(bezAngle*2)*w/2, bezAngle*i)

		bs_bezier_l(bs_rotate(x0*w/2, y0*w/2, bezAngle*i))
		bs_bezier_c(x1, y1, x2, y2, x3, y3)
	end

	local w=w/2-w/2*math.max(0, math.min(line, 1))
	local subBezAg=math.rad(0)

	bs_bezier_m(bs_rotate(x0*w, y0*w, subBezAg))

	for i=0, point-1 do

		local x1, y1=bs_rotate(math.cos(bezAngle*1.33)*retio*w, math.sin(bezAngle*1.33)*retio*w, bezAngle*i+subBezAg)
		local x2, y2=bs_rotate(math.cos(bezAngle*1.66)*retio*w, math.sin(bezAngle*1.66)*retio*w, bezAngle*i+subBezAg)
		local x3, y3=bs_rotate(math.cos(bezAngle*2)*w, math.sin(bezAngle*2)*w, bezAngle*i+subBezAg)

		bs_bezier_l(bs_rotate(x0*w, y0*w, bezAngle*i+subBezAg))
		bs_bezier_c(x1, y1, x2, y2, x3, y3)
	end

	bs_bezier_mul(1, hFlat)
	bs_bezier_rotate(angle)
	bs_bezier_move(x, y)

	bs_fill(r, g, b, a)

	return
end

-- main --
function main(x, y, p)

	local pressInterval=1
	local pressSize=1
	local pressAlpha=1
	local pressDot=1

	if pm[3].v ~= 0 then
		pressInterval=bs_width()/bs_width_max()^(4*pm[3].v/100)*(1-pm[2].v/100)+pm[2].v/100
	end

	if pm[4].v ~= 0 then
		pressSize=p^(4*pm[4].v/100)*(1-bs_width_min()/bs_width_max())+bs_width_min()/bs_width_max()
	end

	if pm[5].v ~= 0 then
		pressAlpha=p^(4*pm[5].v/100)
	end

	if pm[7].v ~= 0 then
		pressDot=p^(4*pm[7].v/100)*(1-pm[6].v/100)+pm[6].v/100
	end

	local w=bs_width_max()*pressSize

	local distance=bs_distance(lastX-x, lastY-y)

	local intervalAdjust=bs_width_max()-(bs_width_max()-w)
	local interval=intervalAdjust/10+intervalAdjust/4*pm[1].v/100+intervalAdjust/2.5*pm[1].v/100*(1-pm[8].v/100)-intervalAdjust/2.5*(1-pressDot)*(1-pm[8].v/100)

	if pm[7].v ~= 0 then
		interval=interval+intervalAdjust/2.25*(1-p^(2*pm[7].v/100))*(1-pm[1].v/100)
	end

	if not firstDraw then
		if distance < interval then
			return 0
		end
	end

	local r, g, b=bs_fore()

	local dotW=w*pressDot*(1-pm[8].v/100)

		local rndAngle=math.rad(bs_grand(0, 180))
		local rndDist=math.random(0, w/2*pm[3].v/100*pressDot)
		local moveAngle=bs_atan(bs_dir())

		local bx=x+math.cos(moveAngle-rndAngle)*rndDist
		local by=y+math.sin(moveAngle-rndAngle)*rndDist

		for i=0, math.min(w, 19)*(1-pm[1].v/100)*pm[8].v/100 do

			local rndInterval=0

			if pm[7].v ~= 0 then
				rndInterval=math.random(100*(1-math.max(p^(10*pm[7].v/100), 0.01))*(1-pm[8].v/100), 100)/100
			end

			if rndInterval < 1 then

				local a=bs_opaque()*255*pressAlpha*(1-math.random(0, pm[10].v)/100)
				local rndAngle=math.rad(bs_grand(0, 180))

				while lastRndAngle == rndAngle do
					rndAngle=math.rad(bs_grand(0, 180))
				end

				local rndDist=math.random(0, w/2*pm[8].v/100)
				local rndSize=(1-math.random(0, pm[2].v)/100)*(1+math.random(0, pm[3].v^(1-pm[8].v/100))/100)
				local rndAg=math.rad(bs_grand(0, 180))

				local xx=math.cos(rndAngle)*rndDist
				local yy=math.sin(rndAngle)*rndDist*(1-pm[3].v/100)

				xx, yy=bs_rotate(xx, yy, rndAg)
				xx, yy=bx+xx, by+yy

				local point=math.random(3, 4)

				if point ~= 4 then
					while lastPoint == point do
						 point=math.random(3, 4)
					end
				end

				local moveAngleN=bs_atan(bs_normal())

				drawPolygon(xx, yy, dotW*rndSize, math.random(50, 100)/100, rndAngle+moveAngleN, point, 0.9*pm[9].v/100, 1, r, g, b, a)

				lastRndAngle=rndAngle
				lastPoint=point
			end
		end

	firstDraw=false
	lastX, lastY=x, y

	return 1
end
